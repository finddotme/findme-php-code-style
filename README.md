### Installation

Add library into section "require-dev" of composer.json

```
"finddotme/findme-php-code-style": "0.0.1"
```

Add script into composer.json

```
"post-autoload-dump": [
    "[ -f ./vendor/bin/code-style-setup.sh ] && ./vendor/bin/code-style-setup.sh || [ ! -f ./vendor/bin/code-style-setup.sh ]"
],
```