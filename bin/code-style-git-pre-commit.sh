#!/bin/sh

PROJECT=`php -r "echo dirname(dirname(dirname(dirname(dirname(realpath('$0'))))));"`
STAGED_FILES_CMD=`git diff --cached --name-only --diff-filter=ACMR HEAD | grep \\\\.php`
BIN_DIR=bin

function printlnGreen(){
    GREEN='\033[0;32m'
    NC='\033[0m' # No Color

    printf "[${GREEN}$1${NC}]\n"
}

function printlnRed(){
    RED='\033[0;31m'
    NC='\033[0m' # No Color

    printf "[${RED}$1${NC}]\n"
}


if [ ! -f $BIN_DIR/phpcs ]
then
    BIN_DIR=vendor/bin
fi

# Determine if a file list is passed
if [ "$#" -eq 1 ]
then
    oIFS=$IFS
    IFS='
    '
    SFILES="$1"
    IFS=$oIFS
fi
SFILES=${SFILES:-$STAGED_FILES_CMD}

printlnGreen "Checking PHP Lint..."
for FILE in $SFILES
do
    php -l -d display_errors=0 $PROJECT/$FILE
    if [ $? != 0 ]
    then
        printlnRed "Fix the error before commit."
        exit 1
    fi
    FILES="$FILES $PROJECT/$FILE"
done

if [ "$FILES" != "" ]
then
    printlnGreen "Running Code Sniffer..."
    ./$BIN_DIR/phpcs --standard=Findme --encoding=utf-8 -n -p -s --colors $FILES
    if [ $? != 0 ]
    then
        printlnRed "Fix the error before commit."
        exit 1
    fi
fi

exit $?