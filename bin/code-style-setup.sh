#!/bin/bash

BIN_DIR=bin

if [ ! -f $BIN_DIR/code-style-git-pre-commit.sh ]
then
    BIN_DIR=vendor/bin
fi

if [[ -z `$BIN_DIR/phpcs -i | grep Symfony2` || -z `$BIN_DIR/phpcs -i | grep findme` ]]
then
    $BIN_DIR/phpcs --config-set installed_paths vendor/escapestudios/symfony2-coding-standard,vendor/finddotme/findme-php-code-style
fi

ln -s -f ../../$BIN_DIR/code-style-git-pre-commit.sh ./.git/hooks/pre-commit
chmod 700 ./.git/hooks/pre-commit
